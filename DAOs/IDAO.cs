﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAOs
{
    interface IDAO<T>
    {
        void incluir(T entidade);
        void excluir(T entidade);
        void atualizar(T entidade);
        T buscarPorId(int id);
        List<T> listar();
    }
}
